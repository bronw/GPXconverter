# -------------------------------------------------------------------------------
# Name:         FeatureToFeatureClass
# Purpose:      Convert each unique feature in a layer into a feature class
#               with just one feature.
#
# Author:      clarkdav
#
# Created:     18/05/2016
# Copyright:   (c) clarkdav 2016
# Licence:     <your licence>
# -------------------------------------------------------------------------------

import arcpy
import os
import re
import traceback

from FeaturesToGPX.FeaturesToGPX import featuresToGPX



def exportFeaturesToFeatureClassByValue(source_FC, fieldName, prefix, dest_WS):
    """ exports features to a set of feature classes

    Features of a layer are exported to aworkspace, based on the unique values
    in the provided field.

    provide:
        source_FC: Source Feature class.
        fieldName: Field containing the unique identifer.
        prefix: A string prefix to ensure that the feature class
                   name does not start with a numeral.
        dest_WS: Destination workspace

    returns:
        a list of name of the new feature classes created
    makes:
        new feature classes in the provided work space.
    """
    try:

        # Break up the source feature class path.
        source_FC_dir = os.path.dirname(source_FC)
        source_FC_name = os.path.basename(source_FC)


        # TODO
        #   - try/throw around identifying the field name
        #   - user input on the unexpectidly high number of uniques

        # populate list of unique values of [field] in record set
        uniqueValues = set([])
        createdFeatureClasses = set([])
        rows = arcpy.SearchCursor(source_FC)
        for row in rows:
            val = row.getValue(fieldName)
            uniqueValues.add(val)

            if len(uniqueValues) > 1000:
                raise Exception('More than 1000 unique values')

        #DEV
        #   - slicing to shortened list here!!!!!!!!!!!!!!!

        # make single polygon fc for each feature (uniqueValues)
        arcpy.AddMessage("DEV - SET OF LAYER NAMES")
        arcpy.AddMessage("DEV!!!! have set to only 10 items")

        DEVlimit = 0
        for val in uniqueValues:
            #DEV
            DEVlimit+= 1
            if DEVlimit>10:
                break

# try   arcpy.validTableName

            # test if chr prefix is required
            if val[0].isdigit():
                layer_name = prefix + re.sub('[ \'\t\n-/]', '_', val)
            else:
                layer_name = re.sub('[ \'\t\n-/]', '_', val)

            #layer_name = arcpy.ValidateTableName(val)



            arcpy.AddMessage(layer_name)
            value = re.sub('\'', '\'\'', val)

            targetLayer = os.path.join(dest_WS, layer_name)
            if arcpy.Exists(targetLayer):
                arcpy.Delete_management(targetLayer)

            arcpy.FeatureClassToFeatureClass_conversion(
                                    source_FC,
                                    dest_WS,
                                    layer_name,
                                    fieldName + ' = \'' + value + '\'')

            createdFeatureClasses.add(layer_name)




    finally:
        # Remove local variables that may hold locks on datasets or
        # may contain large data structures.
        if 'uniqueValues' in locals():
            del uniqueValues

        if 'row' in locals():
            del row

        if 'rows' in locals():
            del rows

        # return
        return createdFeatureClasses




def dessolveFeaturesInList(createdFeatureClasses,fieldName,location_WS):
    """ recieves a set of feature class names and disolves the geometory

    the feature class is simplified by disolve, based on the unique values
    in the provided field.

    provide:
        createdFeatureClasses: set of feature names to work with.
        fieldName: Field containing the unique identifer.
        location_WS: Destination workspace.

    actions:
        makes new feature classes in the provided work space.

    returns:
        a list of name of the new feature classes created
    """

    # record current Workspace (return afterwards)
    WS_old = arcpy.env.workspace

    # Start return variable
    desolvedFeatureClasses = set([])


    arcpy.AddMessage("DEV - DISSOLVE FEATURE")


    try:
        # change workspace
        arcpy.env.workspace = location_WS

        # iterate imported freatures
        for fClassName in createdFeatureClasses:
            newfClassName = "dis_" + fClassName
            targetLayer = os.path.join(location_WS, newfClassName)
            if arcpy.Exists(targetLayer):
                arcpy.Delete_management(targetLayer)

            arcpy.Dissolve_management(fClassName,targetLayer,fieldName)
            desolvedFeatureClasses.add(newfClassName)
            arcpy.AddMessage(newfClassName)

    finally:
        arcpy.env.workspace = WS_old
        return desolvedFeatureClasses





def convertAllPolygonsInWorkspaceToGPX(source_WS,targetFolder):
    """ Batch converts a group of polygon feature classes to GPX files.

    Only works with polygon features each in their own feature class

    provide:
        source_WS - a workspace containing the feature classes
        targetFolder - where GPX will be created
    """

    # record current Workspace (return afterwards)
    WS_old = arcpy.env.workspace
    try:

        # change workspace
        arcpy.env.workspace = source_WS

        #convert polygons
        fc_list = arcpy.ListFeatureClasses (feature_type= 'Polygon')
        arcpy.AddMessage ("DEV - MAKE GPX FILES")

        for fc in fc_list:

            fcFileName= os.path.basename (fc)
            fcFileNameSplit = os.path.splitext(fcFileName)[0]
            target_GPX = os.path.join (targetFolder, fcFileNameSplit + '.gpx')
            convertSinglePolygonFeatureClassToGPX(fc, target_GPX)

            arcpy.AddMessage (fcFileNameSplit)

    finally:
        arcpy.env.workspace = WS_old



def convertPolygonsInSetToGPX(dissolvedFeatureClasses,source_WS,targetFolder):
    """ Batch converts a set of polygon feature classes to GPX files.

    Only works with polygon features each in their own feature class

    provide:
        dissolvedFeatureClasses - a set of feature names
        source_WS - a workspace containing the feature classes
        targetFolder - where GPX will be created
    """

    # record current Workspace (return afterwards)
    arcpy.AddMessage ("DEV - CONVERT TO GPX")
    WS_old = arcpy.env.workspace
    try:

        # change workspace
        arcpy.env.workspace = source_WS



        for fClassName in dissolvedFeatureClasses:

            # get the feature Class
            fc = fClassName
            targetName = fClassName + '.gpx'

            target_GPX = os.path.join (targetFolder, targetName)
            convertSinglePolygonFeatureClassToGPX(fc, target_GPX)

            arcpy.AddMessage (targetName)

    finally:
        arcpy.env.workspace = WS_old




def convertSinglePolygonFeatureClassToGPX(fc, target_GPX):
    """ converts polygon feature class to GPX file

    Provide a polygon feature class with a single feature.
    the is converted into a GPX file

    provide:
        fc - polygon feature class
        target_GPX - path and file name for GPX output
    """
    featuresToGPX (fc, target_GPX, False, True)




def convertPolygonLayerToGPX(source_FC, fieldName,dest_WS,targetFolder,
                                prefix = 'FR_'):
    """Multi Polygons in a layer to multipule GPX files

    Seperates a polygon layer into seperate features classes,
    then converts each classe into a GPX file.

    provide:
        source_FC = polygon feature layer
        fieldName = field to divide into sperate feature classes
        dest_WS   = work space to store the feature classes
        prefix    = A string prefix (default is 'FR_') used to ensure
                    the feature class name does not start with a numeral.
        targetFolder = folder GPX files are experted into
    """

    try:

        # Export the features to seperate feature classes
        createdFeatureClasses = exportFeaturesToFeatureClassByValue(source_FC, fieldName, prefix, dest_WS)

        dissolvedFeatureClasses = dessolveFeaturesInList(createdFeatureClasses,fieldName,dest_WS)
        ## convert new fc to GPX
        ##convertAllPolygonsInWorkspaceToGPX(dest_WS,targetFolder)
        # convert SET OF fc to GPX
        convertPolygonsInSetToGPX(dissolvedFeatureClasses,dest_WS,targetFolder)

    except Exception as e:
        print e.message
        arcpy.AddError(e.message)
        arcpy.AddError(traceback.format_exc())



def main():
    """ runs convertPolygonLayerToGPX() """


    # Retrieve the source feature class.
    source_FC = arcpy.GetParameterAsText(0)
    fieldName = arcpy.GetParameterAsText(1)
    dest_WS = arcpy.GetParameterAsText(2)
    targetFolder = arcpy.GetParameterAsText(3)

    # call the multipule convert function
    convertPolygonLayerToGPX(source_FC=source_FC,
                             fieldName=fieldName,
                             dest_WS=dest_WS,
                             targetFolder=targetFolder)




if __name__ == '__main__':
    main()
